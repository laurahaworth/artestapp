﻿using System.Threading.Tasks;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class LoginScreenManager : MonoBehaviour
{
    [SerializeField] TMP_InputField _emailInput;
    [SerializeField] TMP_InputField _passwordInput;
    [SerializeField] Button _loginButton;
    [SerializeField] ActivityIndicatorView _activityIndicatorView;


    public void Awake()
    {
        _loginButton.interactable = false;
    }

    public void LoginButtonClicked()
    {
        DummyLogin();
    }

    async void DummyLogin()
    {
        _activityIndicatorView.gameObject.SetActive(true);
        _activityIndicatorView.FadeIn();
        await Task.Delay(TimeSpan.FromSeconds(2));

        _activityIndicatorView.ShowSuccess();

        TransitionManager.Instance.LoadSceneWithTransition("LandingScreen");
    }

    //keep unused email parameter - dynamic string parameter
    public void EmailFieldChanged(string email)
    {
        Validate();
    }

    //keep unused email parameter - dynamic string parameter
    public void PasswordFieldChanged(string password)
    {
        Validate();
    }

    private void Validate()
    {
        _loginButton.GetComponent<Button>().interactable = RegexUtilities.IsValidEmail(_emailInput.text) && RegexUtilities.IsValidPassword(_passwordInput.text);
    }

}
