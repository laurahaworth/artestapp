﻿using UnityEngine;

public class LandingScreenManager : TransitioningScreenManager
{

    public void OpenURLButtonClicked()
    {
        Application.OpenURL("http://unity3d.com/");
    }

    public void ARExperienceButtonClicked()
    {
        TransitionManager.Instance.LoadSceneWithTransition("ARScene");
    }
}
