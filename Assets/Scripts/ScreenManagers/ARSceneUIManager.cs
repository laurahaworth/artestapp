﻿using UnityEngine;
using System;

public class ARSceneUIManager: TransitioningScreenManager
{

    public Action PlaceButtonClicked;
    public Action<InteractionMode> InteractionButtonClicked;
    public Action<Model> SelectModelButtonClicked;

    [SerializeField] GameObject _instructionView;

    [SerializeField] GameObject _selectModelController;
    [SerializeField] TableView _modelListTableView;
    [SerializeField] GameObject _cancelModelSelectButton;
    [SerializeField] ImageAspectFill _selectedModelButtonImage;

    [SerializeField] GameObject _interactionButtonView;
    [SerializeField] InteractionButton _moveButton;
    [SerializeField] InteractionButton _rotateButton;
    [SerializeField] InteractionButton _scaleButton;

    [SerializeField] ScanAndPlaceButton _scanAndPlaceButton;

    override protected void Awake()
    {
        base.Awake();
        _interactionButtonView.SetActive(false);
        _selectModelController.SetActive(true);
    }

    override protected void Start()
    {
        base.Start();
        _modelListTableView.SetUp(DataManager.Instance.Models, ModelCellItemSelected);

    }


    public void ShowSelectModelController()
    {
        _selectModelController.SetActive(true);
    }

    public void HideSelectModelController()
    {
        _selectModelController.SetActive(false);
    }


    public void ModelCellItemSelected(ITableViewCellItem selectedItem)
    {
        var selectedModel = (Model)selectedItem;

        _cancelModelSelectButton.SetActive(true);
        _selectModelController.SetActive(false);

        //_selectedModelButtonImage.SetSprite(DataManager.Instance.PlantAssetBundle.LoadAsset<Sprite>(selectedModel.ImagePath));
        _selectedModelButtonImage.SetSprite(Resources.Load<Sprite>("plantassets/Images/" + selectedModel.ImagePath));

        SelectModelButtonClicked.Invoke(selectedModel);
    }

    public void InteractionModeSelected(int interactionModeRaw)
    {
        InteractionMode selectedInteractionMode = (InteractionMode)interactionModeRaw;

        _moveButton.SetInteractionSelected(selectedInteractionMode == InteractionMode.MOVE);
        _rotateButton.SetInteractionSelected(selectedInteractionMode == InteractionMode.ROTATE);
        _scaleButton.SetInteractionSelected(selectedInteractionMode == InteractionMode.SCALE);

        InteractionButtonClicked.Invoke(selectedInteractionMode);
    }

    public void ShowScanFloor()
    {
        _scanAndPlaceButton.ShowScanFloor();
    }

    public void ShowPlaceButton()
    {
        _scanAndPlaceButton.ShowPlaceButton();
    }

    public void OnPlaceButtonClicked()
    {
        PlaceButtonClicked.Invoke();
        ModelPlaced();
    }

    public void ModelPlaced()
    {
        _interactionButtonView.SetActive(true);
        InteractionModeSelected((int)InteractionMode.MOVE);

        _scanAndPlaceButton.gameObject.SetActive(false);

        DisplayInstructionsIfNotAlreadyViewed();
    }

    public void ExitButtonClicked()
    {
        TransitionManager.Instance.LoadSceneWithoutTransision("LandingScreen");
    }

    public void InstructionsButtonClicked()
    {
        _instructionView.gameObject.SetActive(true);
    }

    public void HideInstructions()
    {
        _instructionView.gameObject.SetActive(false);

    }

    public void DisplayInstructionsIfNotAlreadyViewed()
    {
        if (PlayerPrefs.GetInt("Viewed_Instructions") != 1)
        {
            _instructionView.gameObject.SetActive(true);
            PlayerPrefs.SetInt("Viewed_Instructions", 1);
        }
    }
}
