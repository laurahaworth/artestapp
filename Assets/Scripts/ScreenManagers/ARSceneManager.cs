﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;


public enum InteractionMode
{
    NOT_PLACED,
    MOVE,
    ROTATE,
    SCALE
}

public class ARSceneManager : MonoBehaviour
{
    [SerializeField] ARSceneUIManager _uiManager;
    InteractionMode interactionMode;
    GameObject _spawnedObject;
    [SerializeField] ARRaycastManager _raycastManager;

    //scaling interaction helpers
    float _initialDistance;
    Vector3 _initialScale;

    void Start()
    {
        _uiManager.PlaceButtonClicked += HandlePlaceButtonClicked;
        _uiManager.InteractionButtonClicked += HandleInteractionButtonClicked;
        _uiManager.SelectModelButtonClicked += HandleModelSelected;
    }


    void Update()
    {
        if (_spawnedObject == null)
        {
            return;
        }

        if (interactionMode != InteractionMode.NOT_PLACED)
        {
            if (Input.touchCount == 0)
                return;

            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                return;
        }


        switch (interactionMode)
        {
            case InteractionMode.NOT_PLACED:
                ShowPlaceTarget();
                break;

            case InteractionMode.MOVE:
                Move();
                break;

            case InteractionMode.ROTATE:
                Rotate();
                break;

            case InteractionMode.SCALE:
                Scale();
                break;

            default:
                break;
        }
    }


    private void ShowPlaceTarget()
    {

        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        if (_raycastManager.Raycast(new Vector3(Screen.width / 2, Screen.height / 2, 0), hits, TrackableType.PlaneWithinPolygon))
        {
            CheckAndSetTargetVisibility(true);

            var hitPose = hits[0].pose;
            _spawnedObject.transform.position = Vector3.Lerp(_spawnedObject.transform.position, hitPose.position, Time.deltaTime * 6f);
        }
        else
            CheckAndSetTargetVisibility(false);
    }

    private void Move()
    {
        if (Input.touches[0].phase == TouchPhase.Canceled || Input.touches[0].phase == TouchPhase.Ended)
            return;

        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        var touch = Input.GetTouch(0);
        if (_raycastManager.Raycast(touch.position, hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = hits[0].pose;
            _spawnedObject.transform.position = hitPose.position;  
        }
    }

    private void Rotate()
    {
        var touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Moved)
        {
            var rotationY = Quaternion.Euler(0f, -touch.deltaPosition.x * 0.3f, 0f);
            _spawnedObject.transform.rotation *= rotationY;
        }
    }

    private void Scale()
    {
        if (Input.touchCount == 2)
        {
            if (Input.touches[0].phase == TouchPhase.Began || Input.touches[1].phase == TouchPhase.Began)
            {
                _initialDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
                _initialScale = _spawnedObject.transform.localScale;
            }

            else
            {
                var currentDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
                 var factor = currentDistance / _initialDistance;

                if ((_initialScale * factor).x < 0.01) //prevent model getting to small
                    return;

                _spawnedObject.transform.localScale = _initialScale * factor;
            }
        }
    }

    void CheckAndSetTargetVisibility(bool isVisible)
    {
        if (isVisible && (_spawnedObject.activeInHierarchy == false))
        {
            _spawnedObject.SetActive(true);
            _uiManager.ShowPlaceButton();
        }
        else if (!isVisible && (_spawnedObject.activeInHierarchy == true))
        {
            _spawnedObject.SetActive(false);
            _uiManager.ShowScanFloor();
        }
    }


    //UI Manager listeners
    public void HandlePlaceButtonClicked()
    {
        interactionMode = InteractionMode.MOVE;
    }

    public void HandleInteractionButtonClicked(InteractionMode selectedInteractionMode)
    {
        interactionMode = selectedInteractionMode;
    }

    public void HandleModelSelected(Model selectedModel)
    {
        if (_spawnedObject == null)
        {
            InstantiateFirstModel(selectedModel);
            _uiManager.ShowScanFloor();
        }
        else
            ChangeModel(selectedModel);
    }

    public void InstantiateFirstModel(Model model)
    {
        _spawnedObject = Instantiate(model.GetModelPrefab()) as GameObject;
        _spawnedObject.SetActive(false);
    }

    public void ChangeModel(Model model)
    {
        var currentPos = _spawnedObject.transform.position;
        Destroy(_spawnedObject);
        _spawnedObject = Instantiate(model.GetModelPrefab()) as GameObject;
        _spawnedObject.transform.position = currentPos;
    }
}
