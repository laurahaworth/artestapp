﻿using UnityEngine;
using UnityEngine.UI;

public class ActivityIndicatorView : MonoBehaviour
{
    [SerializeField] Animator _animator;
    [SerializeField] GameObject _tickImage;
    [SerializeField] GameObject _spinnerImage;
    [SerializeField] TMPro.TextMeshProUGUI _label;

    [SerializeField] Sprite _activeSpinnerSprite;
    [SerializeField] Sprite _doneSpinnerSprite;

    public void FadeIn()
    {
        _spinnerImage.GetComponent<Animator>().SetTrigger("StartSpinning");
        _animator.SetTrigger("FadeIn");
    }

    public void ShowSuccess()
    {
        _spinnerImage.GetComponent<Image>().sprite = _doneSpinnerSprite;
        _spinnerImage.GetComponent<Animator>().SetTrigger("StopSpinning");
        _tickImage.SetActive(true);
        _label.gameObject.SetActive(false);
    }
}
