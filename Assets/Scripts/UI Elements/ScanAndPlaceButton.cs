﻿using UnityEngine;
using UnityEngine.UI;

public class ScanAndPlaceButton : MonoBehaviour
{
    [SerializeField] TMPro.TextMeshProUGUI _label;
    [SerializeField] Button _button;

    public void ShowScanFloor()
    {
        _button.interactable = false;
        _label.text = "SCAN FLOOR";
        _label.color = Color.white;
    }

    public void ShowPlaceButton()
    {
        _button.interactable = true;
        _label.text = "PLACE MODEL";
        _label.color = Color.black;
    }
}
