﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class TableView : MonoBehaviour
{
    [SerializeField] GameObject _tableViewCellPrefab;

    public void SetUp(ITableViewCellItem[] items, Action<ITableViewCellItem> itemSelected)
    {
        foreach (ITableViewCellItem item in items)
        {
            var tableViewCellGameObject = Instantiate(_tableViewCellPrefab) as GameObject;
            var tableViewCell = tableViewCellGameObject.GetComponent<TableViewCell>();
            var padding = GetComponent<VerticalLayoutGroup>().padding;
            tableViewCell.SetupCell(item, itemSelected, transform.parent.GetComponent<RectTransform>().rect.width - (padding.left + padding.right));
            tableViewCellGameObject.transform.SetParent(transform);
        }
    }
}