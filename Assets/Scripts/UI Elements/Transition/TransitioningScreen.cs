﻿using UnityEngine;
using UnityEngine.UI;


public class TransitioningScreenManager : MonoBehaviour
{
    [SerializeField] protected Canvas _mainCanvas;
    GameObject _transition;

    protected virtual void Awake()
    {
        if (TransitionManager.Instance.expectingTransition == false) return;

        var lastScreenSprite = TransitionManager.Instance.LastScreenSprite();
        _transition = Instantiate(Resources.Load("Transition")) as GameObject;
        _transition.transform.SetParent(_mainCanvas.transform);
        _transition.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        _transition.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, lastScreenSprite.rect.width);
        _transition.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, lastScreenSprite.rect.height);
        _transition.GetComponent<Image>().sprite = lastScreenSprite;

    }

    protected virtual void Start()
    {
        if (TransitionManager.Instance.expectingTransition == false) return;

        _transition.GetComponent<Transition>().FadeIn();
    }
}
