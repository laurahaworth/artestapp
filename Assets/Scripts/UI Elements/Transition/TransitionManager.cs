﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class TransitionManager : Singleton<TransitionManager>
{
    public Texture2D LastScreenTexture2D;
    public bool expectingTransition;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }


    public void LoadSceneWithTransition(string sceneId)
    {
        expectingTransition = true;
        StartCoroutine(TakeScreenShotAndLoadNextScene(sceneId));
    }

    public void LoadSceneWithoutTransision(string sceneId)
    {
        expectingTransition = false;
        SceneManager.LoadScene(sceneId);
    }

    IEnumerator TakeScreenShotAndLoadNextScene(string sceneId)
    {
        yield return new WaitForEndOfFrame();

        LastScreenTexture2D = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        LastScreenTexture2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        LastScreenTexture2D.Apply();

        SceneManager.LoadScene(sceneId);
    }

    public Sprite LastScreenSprite()
    {
       return Sprite.Create(LastScreenTexture2D, new Rect(0.0f, 0.0f, LastScreenTexture2D.width, LastScreenTexture2D.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

}
