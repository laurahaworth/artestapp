﻿using UnityEngine;
using UnityEngine.Events;


public class Transition : MonoBehaviour
{

    [SerializeField] Animator _animator;

    public UnityAction FadeOutComplete;
    public UnityAction FadeInComplete;

    public void OnFadeOutComplete()
    {
        if (FadeOutComplete != null)
            FadeOutComplete.Invoke();
    }

    public void OnFadeInComplete()
    {
        gameObject.SetActive(false);
        if (FadeInComplete != null)
            FadeInComplete.Invoke();
    }

    public void FadeIn()
    {
        _animator.SetTrigger("FadeInTrigger");
    }

    public void FadeOut()
    {
        _animator.SetTrigger("FadeOutTrigger");

    }
}
