﻿using UnityEngine.UI;
using UnityEngine;

public class InteractionButton : MonoBehaviour
{
    [SerializeField] Color _selectedTextColor;
    [SerializeField] TMPro.TextMeshProUGUI _titleButton;
    [SerializeField] Image _image;
    [SerializeField] Sprite _unselectedBackground;
    [SerializeField] Sprite _selectedBackground;

    void Awake()
    {
        _titleButton.color = Color.black;
        _image.sprite = _unselectedBackground;

    }
    public void SetInteractionSelected(bool isSelected)
    {
        _titleButton.color = isSelected ? _selectedTextColor : Color.black;
        _image.sprite = isSelected ? _selectedBackground : _unselectedBackground;
    }
}
