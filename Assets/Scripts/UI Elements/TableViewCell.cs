﻿using UnityEngine;
using System;

public interface ITableViewCellItem
{
    string Title { get; }
    string ImagePath { get; }
}


public class TableViewCell : MonoBehaviour
{
    ITableViewCellItem _item;
    [SerializeField] TMPro.TextMeshProUGUI _titleLabel;
    [SerializeField] ImageAspectFill _image;

    public Action<ITableViewCellItem> _cellSelectedAction;

    void Awake()
    {
        if (_item != null)
            SetupUI();
    }

    public void CellClicked()
    {
        if (_cellSelectedAction != null)
            _cellSelectedAction.Invoke(_item);
    }

    public void SetupUI()
    {
        _titleLabel.text = _item.Title.ToUpper();

        // var sprite = DataManager.Instance.PlantAssetBundle.LoadAsset<Sprite>(_item.ImagePath);
        var sprite = Resources.Load<Sprite>("plantassets/Images/" + _item.ImagePath);

        _image.SetSprite(sprite);
    }

    public void SetupCell(ITableViewCellItem cellData, Action<ITableViewCellItem> cellSelectedHandler, float cellSize)
    {
        _item = cellData;

        _cellSelectedAction += cellSelectedHandler;

        var cellRectTransform = GetComponent<RectTransform>();
        cellRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, cellSize);
        cellRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, cellSize);

        SetupUI();
    }
}
