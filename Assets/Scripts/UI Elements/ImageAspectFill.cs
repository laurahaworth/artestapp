﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AspectRatioFitter))]

public class ImageAspectFill : MonoBehaviour
{
    public void SetSprite(Sprite sprite)
    {
        GetComponent<AspectRatioFitter>().aspectRatio = sprite.bounds.size.x / sprite.bounds.size.y;
        GetComponent<Image>().sprite = sprite;
    }
}
