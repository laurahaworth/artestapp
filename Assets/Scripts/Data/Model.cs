﻿using UnityEngine;

[System.Serializable]
public class Model : ITableViewCellItem
{
    public string modelPrefabName;
    public string modelTitle;
    public string modelImageName;

    Object _prefab;

    //ITableViewCellItem
    public string Title { get { return modelTitle; } }
    public string ImagePath { get { return modelImageName; } }

    public Object GetModelPrefab()
    {
       if (_prefab == null)
            // _prefab = DataManager.Instance.PlantAssetBundle.LoadAsset<GameObject>(modelPrefabName);
            _prefab = Resources.Load<GameObject>("plantassets/Prefabs/" + modelPrefabName);


        return _prefab;
    }
}

[System.Serializable]
public class ModelData
{
    public Model[] allModels;
}

