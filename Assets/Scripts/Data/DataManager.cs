﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class DataManager : Singleton<DataManager>
{
    public Model[] Models { get; private set; }
    string _modelDataFile = "models.json";
    public AssetBundle PlantAssetBundle;

    void Start()
    {
        DontDestroyOnLoad(gameObject);

        LoadTestModels();

        //LoadAllModels();
        //LoadAssetBundles();
    }


    void LoadAllModels()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, _modelDataFile);

        if (File.Exists(filePath))
        {
            string dataAsJSON = File.ReadAllText(filePath);
            ModelData loadedData = JsonUtility.FromJson<ModelData>(dataAsJSON);
            Models = loadedData.allModels;
        }
        else
        {
            LoadTestModels();

            Debug.LogError("Cannot load model data");
        }
    }


    void LoadTestModels()
    {
        TextAsset file = Resources.Load("Data/models") as TextAsset;
        string dataAsJSON = file.ToString();
        ModelData loadedData = JsonUtility.FromJson<ModelData>(dataAsJSON);
        Models = loadedData.allModels;
    }


    void LoadAssetBundles()
    {
        string target = "";

        #if UNITY_IOS
            target = "iOS";
        #endif

        #if UNITY_ANDROID
            target = "Android";
        #endif

        var filePath = Path.Combine(Application.streamingAssetsPath, "AssetBundles/" + target + "/plantassets");
        PlantAssetBundle = AssetBundle.LoadFromFile(filePath);
    }
   
}
