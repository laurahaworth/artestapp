﻿using UnityEngine.SceneManagement;
public class AppManager : Singleton<AppManager>
{

    private bool _userIsLoggedIn = false;

    void Start()
    {
        DontDestroyOnLoad(gameObject);

        if (_userIsLoggedIn)
            SceneManager.LoadScene("LandingScreen");

        else
            SceneManager.LoadScene("LoginScreen");
    }
    
}
